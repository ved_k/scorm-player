const express = require('express');
const ejs = require('ejs');
const app = express();
const port = 3000;

// static files
console.log('__dirname', __dirname);


app.get('/', function (req, res) {
    app.use('/lms', express.static( __dirname + '/courses/NETWORK/lms' ));
    app.use('/story_content', express.static( __dirname + '/courses/NETWORK/story_content' ));
    app.use('/html5', express.static( __dirname + '/courses/NETWORK/html5' ));
    app.use('/mobile', express.static( __dirname + '/courses/NETWORK/mobile' ));
    // Render page using renderFile method
    ejs.renderFile('./courses/NETWORK/index_lms_html5.html', {},
      {}, function (err, template) {
        if (err) {
          throw err;
        } else {
          res.end(template);
        }
      });
});

app.listen(port, function (error) {
    if (error) {
        throw error;
    } else {
        console.log("Server is running at port", port);
    }
});