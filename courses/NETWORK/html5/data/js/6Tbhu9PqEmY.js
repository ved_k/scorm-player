window.globalProvideData('slide', '{"title":"AND BEFORE WE BEGIN…","trackViews":true,"showMenuResultIcon":false,"viewGroupId":"","historyGroupId":"","videoZoom":"","scrolling":false,"transition":"appear","transDuration":0,"transDir":1,"wipeTrans":false,"slideLock":false,"navIndex":-1,"globalAudioId":"","thumbnailid":"","slideNumberInScene":3,"includeInSlideCounts":true,"presenterRef":{"id":"none"},"showAnimationId":"","lmsId":"Slide3","width":960,"height":540,"resume":false,"background":{"type":"fill","fill":{"type":"linear","rotation":90,"colors":[{"kind":"color","rgb":"0xFFFFFF","alpha":100,"stop":0}]}},"id":"6Tbhu9PqEmY","events":[{"kind":"onbeforeslidein","actions":[{"kind":"if_action","condition":{"statement":{"kind":"compare","operator":"eq","valuea":"$WindowId","typea":"property","valueb":"_frame","typeb":"string"}},"thenActions":[{"kind":"set_frame_layout","name":"npnxnanbnsnfns10110100001"}],"elseActions":[{"kind":"set_window_control_layout","name":"npnxnanbnsnfns10110100001"}]}]},{"kind":"ontransitionin","actions":[{"kind":"adjustvar","variable":"_player.LastSlideViewed_6j0fFQdY8Dr","operator":"set","value":{"type":"string","value":"_player."}},{"kind":"adjustvar","variable":"_player.LastSlideViewed_6j0fFQdY8Dr","operator":"add","value":{"type":"property","value":"$AbsoluteId"}}]}],"slideLayers":[{"enableSeek":true,"enableReplay":true,"timeline":{"duration":5000,"events":[{"kind":"ontimelinetick","time":0,"actions":[{"kind":"show","transition":"appear","objRef":{"type":"string","value":"5u1DhWz46hm"}},{"kind":"show","transition":"appear","objRef":{"type":"string","value":"6mdQpftcPSF"}},{"kind":"show","transition":"appear","objRef":{"type":"string","value":"6Gu6Aghfx9R"}},{"kind":"show","transition":"appear","objRef":{"type":"string","value":"6hmpKmYUkrO"}},{"kind":"show","transition":"appear","objRef":{"type":"string","value":"62Pxcmxpldd"}},{"kind":"show","transition":"appear","objRef":{"type":"string","value":"6lGDk1Obtta"}},{"kind":"show","transition":"appear","objRef":{"type":"string","value":"6lGDk1Obtta.5bR0MJgrdiF"}},{"kind":"show","transition":"appear","objRef":{"type":"string","value":"6lGDk1Obtta.5xke7rf3X0g"}},{"kind":"show","transition":"appear","objRef":{"type":"string","value":"5wggx6PMctw"}},{"kind":"show","transition":"appear","objRef":{"type":"string","value":"678vGb4FEqu"}},{"kind":"show","transition":"appear","objRef":{"type":"string","value":"5jb3ONpJVvS"}}]},{"kind":"ontimelinetick","time":250,"actions":[{"kind":"show","transition":"custom","animationId":"Entrance","reverse":false,"objRef":{"type":"string","value":"6g0jXxcuFPm"}}]}]},"objects":[{"kind":"vectorshape","rotation":0,"accType":"text","cliptobounds":false,"defaultAction":"","shapemaskId":"","xPos":0,"yPos":500,"tabIndex":8,"tabEnabled":true,"xOffset":0,"yOffset":0,"rotateXPos":480,"rotateYPos":20,"scaleX":100,"scaleY":100,"alpha":100,"depth":1,"scrolling":false,"shuffleLock":false,"data":{"hotlinkId":"","accState":0,"vectorData":{"left":0,"top":0,"right":960,"bottom":40,"altText":"Rectangle 2","pngfb":false,"pr":{"l":"Lib","i":0}},"html5data":{"xPos":0,"yPos":0,"width":960,"height":40,"strokewidth":0}},"width":960,"height":40,"resume":false,"useHandCursor":true,"id":"5wggx6PMctw"},{"kind":"vectorshape","rotation":0,"accType":"text","cliptobounds":false,"defaultAction":"","shapemaskId":"","xPos":0,"yPos":0,"tabIndex":0,"tabEnabled":true,"xOffset":0,"yOffset":0,"rotateXPos":480,"rotateYPos":5,"scaleX":100,"scaleY":100,"alpha":100,"depth":2,"scrolling":false,"shuffleLock":false,"data":{"hotlinkId":"","accState":0,"vectorData":{"left":0,"top":0,"right":960,"bottom":10,"altText":"Rectangle 1","pngfb":false,"pr":{"l":"Lib","i":1}},"html5data":{"xPos":0,"yPos":0,"width":960,"height":10,"strokewidth":0}},"width":960,"height":10,"resume":false,"useHandCursor":true,"id":"678vGb4FEqu"},{"kind":"vectorshape","rotation":0,"accType":"image","cliptobounds":false,"defaultAction":"","imagelib":[{"kind":"imagedata","assetId":0,"id":"01","url":"story_content/6baAUgsgDsz_60_DX304_DY304.swf","type":"normal","altText":"Image 13.emf","width":304,"height":57,"mobiledx":0,"mobiledy":0}],"shapemaskId":"","xPos":16,"yPos":514,"tabIndex":15,"tabEnabled":true,"xOffset":0,"yOffset":0,"rotateXPos":52.5,"rotateYPos":10,"scaleX":100,"scaleY":100,"alpha":100,"depth":3,"scrolling":false,"shuffleLock":false,"data":{"hotlinkId":"","accState":0,"vectorData":{"left":0,"top":0,"right":105,"bottom":20,"altText":"Image 13.emf","pngfb":false,"pr":{"l":"Lib","i":2}},"html5data":{"xPos":0,"yPos":0,"width":105,"height":20,"strokewidth":0}},"width":105,"height":20,"resume":false,"useHandCursor":true,"id":"5jb3ONpJVvS"},{"kind":"vectorshape","rotation":0,"accType":"text","cliptobounds":false,"defaultAction":"","shapemaskId":"","xPos":24,"yPos":62,"tabIndex":2,"tabEnabled":true,"xOffset":0,"yOffset":0,"rotateXPos":439.5,"rotateYPos":213.5,"scaleX":100,"scaleY":100,"alpha":100,"depth":4,"scrolling":true,"shuffleLock":false,"data":{"hotlinkId":"","accState":0,"vectorData":{"left":-1,"top":-1,"right":881,"bottom":429,"altText":"Rectangle 4","pngfb":false,"pr":{"l":"Lib","i":42}},"html5data":{"xPos":0,"yPos":0,"width":880,"height":428,"strokewidth":1}},"width":880,"height":428,"resume":false,"useHandCursor":true,"id":"5u1DhWz46hm"},{"kind":"vectorshape","rotation":0,"accType":"image","cliptobounds":false,"defaultAction":"","imagelib":[{"kind":"imagedata","assetId":224,"id":"01","url":"story_content/6VZOzrogTSD_60_P_239_0_753_916_DX996_DY996.swf","type":"normal","altText":"iStock-1031924616.jpg","width":753,"height":916,"mobiledx":0,"mobiledy":0}],"shapemaskId":"","xPos":576,"yPos":63,"tabIndex":3,"tabEnabled":true,"xOffset":0,"yOffset":0,"rotateXPos":164,"rotateYPos":214,"scaleX":100,"scaleY":100,"alpha":100,"depth":5,"scrolling":true,"shuffleLock":false,"data":{"hotlinkId":"","accState":0,"vectorData":{"left":0,"top":0,"right":328,"bottom":428,"altText":"iStock-1031924616.jpg","pngfb":false,"pr":{"l":"Lib","i":1302}},"html5data":{"xPos":0,"yPos":0,"width":328,"height":428,"strokewidth":0}},"width":328,"height":428,"resume":false,"useHandCursor":true,"id":"6mdQpftcPSF"},{"kind":"stategroup","objects":[{"kind":"vectorshape","rotation":0,"accType":"button","cliptobounds":false,"defaultAction":"onrelease","shapemaskId":"","xPos":0,"yPos":0,"tabIndex":9,"tabEnabled":true,"xOffset":0,"yOffset":0,"rotateXPos":16,"rotateYPos":16,"scaleX":100,"scaleY":100,"alpha":100,"depth":1,"scrolling":true,"shuffleLock":false,"data":{"hotlinkId":"","accState":0,"vectorData":{"left":0,"top":0,"right":32,"bottom":32,"altText":"Oval 2","pngfb":false,"pr":{"l":"Lib","i":5}},"html5data":{"xPos":0,"yPos":0,"width":32,"height":32,"strokewidth":0}},"states":[{"kind":"state","name":"_default_Disabled","data":{"hotlinkId":"","accState":1,"vectorData":{"left":0,"top":0,"right":32,"bottom":32,"altText":"Oval 1","pngfb":false,"pr":{"l":"Lib","i":5}},"html5data":{"xPos":0,"yPos":0,"width":32,"height":32,"strokewidth":0}}}],"width":32,"height":32,"resume":false,"useHandCursor":true,"id":"6Gu6Aghfx9R"},{"kind":"vectorshape","rotation":270,"accType":"text","cliptobounds":false,"defaultAction":"","shapemaskId":"","xPos":8,"yPos":12,"tabIndex":11,"tabEnabled":true,"xOffset":0,"yOffset":0,"rotateXPos":8.5,"rotateYPos":7.5,"scaleX":100,"scaleY":100,"alpha":100,"depth":2,"scrolling":true,"shuffleLock":false,"data":{"hotlinkId":"","accState":0,"vectorData":{"left":0,"top":0,"right":18,"bottom":15,"altText":"Triangle 1","pngfb":false,"pr":{"l":"Lib","i":18}},"html5data":{"xPos":0,"yPos":0,"width":18,"height":15,"strokewidth":0}},"width":17,"height":15,"resume":false,"useHandCursor":true,"id":"6MMIkhWZOTd"},{"kind":"vectorshape","rotation":270,"accType":"text","cliptobounds":false,"defaultAction":"","shapemaskId":"","xPos":6,"yPos":8,"tabIndex":10,"tabEnabled":true,"xOffset":0,"yOffset":0,"rotateXPos":8.5,"rotateYPos":7.5,"scaleX":100,"scaleY":100,"alpha":100,"depth":3,"scrolling":true,"shuffleLock":false,"data":{"hotlinkId":"","accState":0,"vectorData":{"left":0,"top":0,"right":18,"bottom":15,"altText":"Triangle 1","pngfb":false,"pr":{"l":"Lib","i":19}},"html5data":{"xPos":0,"yPos":0,"width":18,"height":15,"strokewidth":0}},"width":17,"height":15,"resume":false,"useHandCursor":true,"id":"6KevnC9ZdNx"}],"actionstates":[{"kind":"state","name":"_default","actions":[{"kind":"setobjstate","stateRef":{"type":"string","value":"_default"},"objRef":{"type":"string","value":"6Gu6Aghfx9R"}}],"clickdef":[{"kind":"objref","type":"string","value":"6Gu6Aghfx9R"},{"kind":"objref","type":"string","value":"6MMIkhWZOTd"},{"kind":"objref","type":"string","value":"6KevnC9ZdNx"}]},{"kind":"state","name":"_default_Disabled","actions":[{"kind":"setobjstate","stateRef":{"type":"string","value":"_default_Disabled"},"objRef":{"type":"string","value":"6Gu6Aghfx9R"}}],"clickdef":[{"kind":"objref","type":"string","value":"6Gu6Aghfx9R"},{"kind":"objref","type":"string","value":"6MMIkhWZOTd"},{"kind":"objref","type":"string","value":"6KevnC9ZdNx"}]}],"shapemaskId":"","xPos":848,"yPos":503,"tabIndex":16,"tabEnabled":false,"xOffset":0,"yOffset":0,"rotateXPos":16,"rotateYPos":16,"scaleX":100,"scaleY":100,"alpha":100,"rotation":0,"depth":6,"scrolling":true,"shuffleLock":false,"width":32,"height":32,"resume":false,"useHandCursor":true,"id":"6Gu6Aghfx9R","variables":[{"kind":"variable","name":"_disabled","type":"boolean","value":false,"resume":true},{"kind":"variable","name":"_state","type":"string","value":"_default","resume":true},{"kind":"variable","name":"_stateName","type":"string","value":"","resume":true},{"kind":"variable","name":"_tempStateName","type":"string","value":"","resume":false}],"actionGroups":{"ActGrpSetDisabledState":{"kind":"actiongroup","actions":[{"kind":"adjustvar","variable":"_disabled","operator":"set","value":{"type":"boolean","value":true}},{"kind":"exe_actiongroup","id":"_player._setstates","scopeRef":{"type":"string","value":"_this"}}]},"ActGrpClearStateVars":{"kind":"actiongroup","actions":[{"kind":"adjustvar","variable":"_disabled","operator":"set","value":{"type":"boolean","value":false}}]},"ActGrpSetStatesFinal":{"kind":"actiongroup","actions":[{"kind":"if_action","condition":{"statement":{"kind":"and","statements":[{"kind":"compare","operator":"eq","valuea":"#_state","typea":"var","valueb":"_default","typeb":"string"},{"kind":"compare","operator":"eq","valuea":"6Gu6Aghfx9R.$OnStage","typea":"property","valueb":true,"typeb":"boolean"}]}},"thenActions":[{"kind":"show","transition":"appear","objRef":{"type":"string","value":"6MMIkhWZOTd"}},{"kind":"show","transition":"appear","objRef":{"type":"string","value":"6KevnC9ZdNx"}}],"elseActions":[{"kind":"hide","transition":"appear","objRef":{"type":"string","value":"6MMIkhWZOTd"}},{"kind":"hide","transition":"appear","objRef":{"type":"string","value":"6KevnC9ZdNx"}}]}]},"_show":{"kind":"actiongroup","actions":[{"kind":"show","transition":"appear","objRef":{"type":"string","value":"6Gu6Aghfx9R"}}]}},"events":[{"kind":"onrelease","actions":[{"kind":"gotoplay","window":"_current","wndtype":"normal","objRef":{"type":"string","value":"_player.60c3Gz5PmnF.5pvjR0FHAgn"}}]},{"kind":"ontransitionin","actions":[{"kind":"exe_actiongroup","id":"_show"},{"kind":"exe_actiongroup","id":"_player._setstates","scopeRef":{"type":"string","value":"_this"}}]}]},{"kind":"vectorshape","rotation":0,"accType":"text","cliptobounds":false,"defaultAction":"","textLib":[{"kind":"textdata","uniqueId":"6hmpKmYUkrO_-519983791","id":"01","linkId":"txt__default_6hmpKmYUkrO","type":"vectortext","xPos":0,"yPos":0,"width":0,"height":0,"shadowIndex":-1,"vectortext":{"left":0,"top":0,"right":311,"bottom":42,"pngfb":false,"pr":{"l":"Lib","i":1303}}}],"shapemaskId":"","xPos":24,"yPos":15,"tabIndex":1,"tabEnabled":true,"xOffset":0,"yOffset":0,"rotateXPos":455.5,"rotateYPos":21.5,"scaleX":100,"scaleY":100,"alpha":100,"depth":7,"scrolling":true,"shuffleLock":false,"data":{"hotlinkId":"","accState":0,"vectorData":{"left":0,"top":0,"right":912,"bottom":43,"altText":"AND BEFORE WE BEGIN…","pngfb":false,"pr":{"l":"Lib","i":12}},"html5data":{"xPos":0,"yPos":0,"width":912,"height":43,"strokewidth":0}},"width":911,"height":43,"resume":false,"useHandCursor":true,"id":"6hmpKmYUkrO"},{"kind":"vectorshape","rotation":0,"accType":"text","cliptobounds":false,"defaultAction":"","textLib":[{"kind":"textdata","uniqueId":"6g0jXxcuFPm_-748029518","id":"01","linkId":"txt__default_6g0jXxcuFPm","type":"vectortext","xPos":0,"yPos":0,"width":0,"height":0,"shadowIndex":-1,"vectortext":{"left":0,"top":0,"right":508,"bottom":338,"pngfb":false,"pr":{"l":"Lib","i":1305}}}],"shapemaskId":"","xPos":32,"yPos":73,"tabIndex":4,"tabEnabled":true,"xOffset":0,"yOffset":0,"rotateXPos":260,"rotateYPos":164,"scaleX":100,"scaleY":100,"alpha":100,"depth":8,"scrolling":true,"shuffleLock":false,"data":{"hotlinkId":"","accState":0,"vectorData":{"left":0,"top":0,"right":520,"bottom":338,"altText":"All the characters used in the scenario are fictitious and have no resemblance in real life.\\n\\nMake sure to look for minor details like time stamps, quick tips etc. at the end of each question for better understanding.\\n\\nContact details mentioned in the course are relevant at the time of this course creation and hence, are subject to changes.\\n\\nThe course contains embedded links, documents etc., which are highly recommended for better understanding.\\n\\nIn case of any query or further guidance, please feel free to consult your department head or BPE organization.\\n","pngfb":false,"pr":{"l":"Lib","i":1304}},"html5data":{"xPos":0,"yPos":0,"width":520,"height":328,"strokewidth":0}},"animations":[{"kind":"animation","id":"Entrance","duration":1250,"hidetextatstart":true,"animateshapewithtext":false,"tweens":[{"kind":"tween","time":0,"duration":1250,"alpha":{"path":[{"kind":"segment","start":"0","dstart":"0","end":"100","dend":"0"}],"duration":1250,"easing":"linear","easingdir":"easein"}}]}],"width":520,"height":328,"resume":false,"useHandCursor":true,"id":"6g0jXxcuFPm"},{"kind":"stategroup","objects":[{"kind":"vectorshape","rotation":0,"accType":"button","cliptobounds":false,"defaultAction":"onrelease","shapemaskId":"","xPos":0,"yPos":0,"tabIndex":12,"tabEnabled":true,"xOffset":0,"yOffset":0,"rotateXPos":16,"rotateYPos":16,"scaleX":100,"scaleY":100,"alpha":100,"depth":1,"scrolling":true,"shuffleLock":false,"data":{"hotlinkId":"","accState":0,"vectorData":{"left":0,"top":0,"right":32,"bottom":32,"altText":"Oval 2","pngfb":false,"pr":{"l":"Lib","i":5}},"html5data":{"xPos":0,"yPos":0,"width":32,"height":32,"strokewidth":0}},"width":32,"height":32,"resume":false,"useHandCursor":true,"id":"62Pxcmxpldd"},{"kind":"vectorshape","rotation":270,"accType":"text","cliptobounds":false,"defaultAction":"","shapemaskId":"","xPos":7,"yPos":11,"tabIndex":14,"tabEnabled":true,"xOffset":0,"yOffset":0,"rotateXPos":8.5,"rotateYPos":7.5,"scaleX":100,"scaleY":100,"alpha":100,"depth":2,"scrolling":true,"shuffleLock":false,"data":{"hotlinkId":"","accState":0,"vectorData":{"left":0,"top":0,"right":18,"bottom":16,"altText":"Triangle 1","pngfb":false,"pr":{"l":"Lib","i":6}},"html5data":{"xPos":0,"yPos":0,"width":18,"height":15,"strokewidth":0}},"width":17,"height":15,"resume":false,"useHandCursor":true,"id":"6JoP2WLdVRR"},{"kind":"vectorshape","rotation":270,"accType":"text","cliptobounds":false,"defaultAction":"","shapemaskId":"","xPos":9,"yPos":8,"tabIndex":13,"tabEnabled":true,"xOffset":0,"yOffset":0,"rotateXPos":8.5,"rotateYPos":7.5,"scaleX":100,"scaleY":100,"alpha":100,"depth":3,"scrolling":true,"shuffleLock":false,"data":{"hotlinkId":"","accState":0,"vectorData":{"left":0,"top":0,"right":18,"bottom":16,"altText":"Triangle 1","pngfb":false,"pr":{"l":"Lib","i":7}},"html5data":{"xPos":0,"yPos":0,"width":18,"height":15,"strokewidth":0}},"width":17,"height":15,"resume":false,"useHandCursor":true,"id":"63mR5MIeu60"}],"actionstates":[{"kind":"state","name":"_default","actions":[{"kind":"setobjstate","stateRef":{"type":"string","value":"_default"},"objRef":{"type":"string","value":"62Pxcmxpldd"}}],"clickdef":[{"kind":"objref","type":"string","value":"62Pxcmxpldd"},{"kind":"objref","type":"string","value":"6JoP2WLdVRR"},{"kind":"objref","type":"string","value":"63mR5MIeu60"}]}],"shapemaskId":"","xPos":895,"yPos":503,"tabIndex":17,"tabEnabled":false,"xOffset":0,"yOffset":0,"rotateXPos":16,"rotateYPos":16,"scaleX":100,"scaleY":100,"alpha":100,"rotation":0,"depth":9,"scrolling":true,"shuffleLock":false,"width":32,"height":32,"resume":false,"useHandCursor":true,"id":"62Pxcmxpldd","variables":[{"kind":"variable","name":"_state","type":"string","value":"_default","resume":true},{"kind":"variable","name":"_disabled","type":"boolean","value":false,"resume":true},{"kind":"variable","name":"_stateName","type":"string","value":"","resume":true},{"kind":"variable","name":"_tempStateName","type":"string","value":"","resume":false}],"actionGroups":{"ActGrpClearStateVars":{"kind":"actiongroup","actions":[]},"ActGrpSetStatesFinal":{"kind":"actiongroup","actions":[{"kind":"if_action","condition":{"statement":{"kind":"and","statements":[{"kind":"compare","operator":"eq","valuea":"#_state","typea":"var","valueb":"_default","typeb":"string"},{"kind":"compare","operator":"eq","valuea":"62Pxcmxpldd.$OnStage","typea":"property","valueb":true,"typeb":"boolean"}]}},"thenActions":[{"kind":"show","transition":"appear","objRef":{"type":"string","value":"6JoP2WLdVRR"}},{"kind":"show","transition":"appear","objRef":{"type":"string","value":"63mR5MIeu60"}}],"elseActions":[{"kind":"hide","transition":"appear","objRef":{"type":"string","value":"6JoP2WLdVRR"}},{"kind":"hide","transition":"appear","objRef":{"type":"string","value":"63mR5MIeu60"}}]}]},"_show":{"kind":"actiongroup","actions":[{"kind":"show","transition":"appear","objRef":{"type":"string","value":"62Pxcmxpldd"}}]}},"events":[{"kind":"onrelease","actions":[{"kind":"gotoplay","window":"_current","wndtype":"normal","objRef":{"type":"string","value":"_player.60c3Gz5PmnF.6Wuiyp9kHqD"}}]},{"kind":"ontransitionin","actions":[{"kind":"exe_actiongroup","id":"_show"},{"kind":"exe_actiongroup","id":"_player._setstates","scopeRef":{"type":"string","value":"_this"}}]}]},{"kind":"objgroup","objects":[{"kind":"vectorshape","rotation":0,"accType":"text","cliptobounds":false,"defaultAction":"","textLib":[{"kind":"textdata","uniqueId":"5bR0MJgrdiF_-775549060","id":"01","linkId":"txt__default_5bR0MJgrdiF","type":"vectortext","xPos":0,"yPos":0,"width":0,"height":0,"shadowIndex":-1,"vectortext":{"left":0,"top":0,"right":229,"bottom":30,"pngfb":false,"pr":{"l":"Lib","i":9}}}],"shapemaskId":"","xPos":0,"yPos":0,"tabIndex":6,"tabEnabled":true,"xOffset":0,"yOffset":0,"rotateXPos":125,"rotateYPos":21,"scaleX":100,"scaleY":100,"alpha":100,"depth":1,"scrolling":true,"shuffleLock":false,"data":{"hotlinkId":"","accState":0,"vectorData":{"left":0,"top":0,"right":250,"bottom":42,"altText":"Click the Play (   ) button to proceed.","pngfb":false,"pr":{"l":"Lib","i":8}},"html5data":{"xPos":0,"yPos":0,"width":250,"height":42,"strokewidth":0}},"width":250,"height":42,"resume":false,"useHandCursor":true,"id":"5bR0MJgrdiF"},{"kind":"vectorshape","rotation":90,"accType":"text","cliptobounds":false,"defaultAction":"","shapemaskId":"","xPos":104,"yPos":18,"tabIndex":7,"tabEnabled":true,"xOffset":0,"yOffset":0,"rotateXPos":4.5,"rotateYPos":4,"scaleX":100,"scaleY":100,"alpha":100,"depth":2,"scrolling":true,"shuffleLock":false,"data":{"hotlinkId":"","accState":0,"vectorData":{"left":0,"top":0,"right":9,"bottom":8,"altText":"Triangle 1","pngfb":false,"pr":{"l":"Lib","i":10}},"html5data":{"xPos":0,"yPos":0,"width":9,"height":8,"strokewidth":0}},"width":9,"height":8,"resume":false,"useHandCursor":true,"id":"5xke7rf3X0g"}],"accType":"text","altText":"Group\\r\\n 1","shapemaskId":"","xPos":606,"yPos":499,"tabIndex":5,"tabEnabled":true,"xOffset":0,"yOffset":0,"rotateXPos":125,"rotateYPos":21,"scaleX":100,"scaleY":100,"alpha":100,"rotation":0,"depth":10,"scrolling":true,"shuffleLock":false,"width":250,"height":42,"resume":false,"useHandCursor":true,"id":"6lGDk1Obtta"}],"startTime":-1,"elapsedTimeMode":"normal","useHandCursor":false,"resume":false,"kind":"slidelayer","isBaseLayer":true}]}');